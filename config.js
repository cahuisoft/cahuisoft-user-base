const config = {
  database: {
      host: 'localhost',
      user: 'user',
      password: 'pass',
      database: 'userbase',
      connectionLimit: 10
    }
}

module.exports = config;