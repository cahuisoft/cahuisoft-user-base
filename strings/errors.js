const errors = {
  incorrectLogin: '[1001] El login es incorrecto, verificar el usuario y contraseña proporcionados',
  errorAddUser: '[1002] No fue posible agregar al usuario, verifica la sintaxis en el metodo en userDAO.addUser',
  errorDataEmpty: '[1003] Se mandaron datos vacios, favor de verificar los datos del login proporcionados',
  errorUpdateUser: '[1004] No fue posible actualizar los datos del usuario, verifica la sintaxis en el metodo en userDAO.updateUser',
  errorTypeIdDelete: '[1005] El id proporcionado no es numerico',
  errorNoDeleteId: '[1006] No existe ningun usuario con el id proporcionado'
}

module.exports = errors;