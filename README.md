# Base de aplicaciones en node

## Informacion
Este repositorio es la base de proyectos de node con el modulo de usuarios precargado

## Instrucciones

1. Ejecuta 
``` 
npm install 
```
2. En el archivo config.js agrega los datos de tu base de datos
3. Corre el script localizado en SQL/db.sql en tu gestor de base de datos MySQL
4. Para levantar el servidor, ejecuta 
``` 
npm start 
``` 


Puedes visitar la wiki del repositorio para la documentacion.