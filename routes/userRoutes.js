const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');

router.get('/auth', userController.auth);
router.post('/add', userController.addUser);
router.put('/update/:userId', userController.updateUser);
router.delete('/delete/:userId', userController.deleteUser);


module.exports = router;