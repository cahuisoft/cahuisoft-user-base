const userDAO = require('../DAO/userDAO');
const userValidator = require('../tools/userValidator');
const errorMessages = require('../strings/errors');

class userService
{
  validateLogin(data)
  {
    if(userValidator.dataAuthValidator(data))
    {
      return userDAO.validateUser(data.userEmail,data.userPassword);
    }
    else
    {
      throw new Error(errorMessages.errorDataEmpty);
    }
    
  }

  addUser(dataUser)
  {
    if(userValidator.dataRegisterValidator(dataUser))
    {
      return userDAO.addUser(dataUser);
    }
    else
    {
      throw new Error(errorMessages.errorDataEmpty);
    }
  }

  updateUserData(userData,idUser)
  {
    if(userValidator.dataUpdateValidator(userData,idUser))
    {
      return userDAO.updateUser(idUser,userData);
    }
    else
    {
      throw new Error(errorMessages.errorDataEmpty);
    }
  }

  deleteUserData(idUser)
  {
    let userId = parseInt(idUser);
    
    if(userValidator.dataDeleteValidator(userId))
    {
      return userDAO.deleteUser(userId);
    }
    else
    {
      throw new Error(errorMessages.errorTypeIdDelete);
    }
  }
}

module.exports = new userService();