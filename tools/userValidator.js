class userValidate
{
  dataAuthValidator(data)
  {
    let hasErrors = false;

    if(data === null ||
       data === undefined ||
       data.userEmail === null ||
       data.userEmail === undefined ||
       data.userEmail.trim() === '' ||
       data.userPassword === null ||
       data.userPassword === undefined ||
       data.userPassword.trim() === ''){
       hasErrors = true;
    }

    if(hasErrors)
    {
      return false;
    }

    return true;
  }

  dataRegisterValidator(data)
  {
    let hasErrors = false;

    if(data === null ||
       data === undefined ||
       data.userName === null ||
       data.userName === undefined ||
       data.userName.trim() === '' ||
       data.userEmail === null ||
       data.userEmail === undefined ||
       data.userEmail.trim() === '' ||
       data.userPassword === null ||
       data.userPassword === undefined ||
       data.userPassword.trim() === ''){
       hasErrors = true;
    }

    if(hasErrors)
    {
      return false;
    }

    return true;
  }

  dataUpdateValidator(data,id)
  {
    let hasErrors = false;

    if(data === null ||
       data === undefined ||
       data.userName === null ||
       data.userName === undefined ||
       data.userName.trim() === '' ||
       data.userEmail === null ||
       data.userEmail === undefined ||
       data.userEmail.trim() === '' ||
       data.userPassword === null ||
       data.userPassword === undefined ||
       data.userPassword.trim() === '' ||
       id === null ||
       id === undefined ||
       id <= 0 ){
       hasErrors = true;
    }

    if(hasErrors)
    {
      return false;
    }

    return true;
  }

  dataDeleteValidator(data)
  {
    return typeof data === 'number';
  }
}

module.exports = new userValidate();