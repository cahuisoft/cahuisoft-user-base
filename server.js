const express = require('express');
const morgan = require('morgan');
const port = 8081;
const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, X-Token');
  res.header('Access-Control-Request-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, X-Token');
  res.header('Access-Control-Request-Method', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Expose-Headers', 'X-Token');

  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

const mainRoutes = require('./routes/mainRoutes');
const userRoutes = require('./routes/userRoutes');

app.use(morgan('dev'));
app.use(express.urlencoded({extended : false}));

// EndPoints
app.use('/',mainRoutes);
app.use('/user', userRoutes);


app.listen(port,() =>{
  console.log(`UserBase app listening in port ${port}`);
});