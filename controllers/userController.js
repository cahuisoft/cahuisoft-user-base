const userService = require('../services/userService');
class userController
{
  async auth(req,res)
  {
    let body = req.body; //recibe user y password

    try
    {
      let infoUser = await userService.validateLogin(body);
      res.status(200).json({
        success: true,
        data: infoUser
      });
    }
    catch(error)
    {
      console.log('Error: ',error)
      res.status(400).json({
        success: false,
        message: 'User doesn´t exist'
      });
    }
  }

  async addUser(req,res)
  {
    let dataUser = req.body; //userName, userEmail and userPassword

    try
    {
      let newUser = await userService.addUser(dataUser);
      res.status(200).json({
        success: true,
        message: `The user ${dataUser.userName} has been added`
      });
    }
    catch(error)
    {
      console.log('Error: ',error);
      res.status(400).json({
        success: false,
        errorMessage: `${error}`, 
        errorDetail: error
      });
    }
  }

  async updateUser(req,res)
  {
    let userId = req.params.userId;
    let dataUser = req.body;
    try 
    {
      let userUpdate = await userService.updateUserData(dataUser,userId);
      res.status(200).json({
        success: true,
        message: `The user's data is update`
      });
    }
    catch(error)
    {
      res.status(400).json({
        success: false,
        errorMessage: `${error}`,
        errorDetail: error
      });
    }
  }

  async deleteUser(req,res)
  {
    let userId = req.params.userId;
    try 
    {
      let userDelete = await userService.deleteUserData(userId);
      res.status(200).json({
        success: true,
        message: `The user's has been delete`
      });
    }
    catch(error)
    {
      res.status(400).json({
        success: false,
        errorMessage: `${error}`,
        errorDetail: error
      });
    }
  }
}
module.exports = new userController();