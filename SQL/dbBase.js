const mysql = require('mysql');
const util = require('util');
const config = require('../config');

class dbBase
{
  constructor()
  {
    this.pool = mysql.createPool({
      connectionLimit: config.database.connectionLimit,
      host: config.database.host,
      user: config.database.user,
      password: config.database.password,
      database: config.database.database
    });

    this.pool.query = util.promisify(this.pool.query);
  }

  getPool()
  {
    return this.pool;
  }
}

module.exports = dbBase;