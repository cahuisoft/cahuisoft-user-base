const DbBase = require('../SQL/dbBase');
const messages = require('../strings/errors');

class userDAO extends DbBase
{
  async validateUser(user,password)
  {
    let result = await this.pool.query(
      'SELECT * FROM user WHERE user_email = ? AND user_password = ?',
      [user,password]);

    if(result.length === 0)
    {
      throw new Error(messages.incorrectLogin);
    }

    return result[0];
  }

  async addUser(userData)
  {
    let result = await this.pool.query(
      'INSERT INTO user(user_name,user_email,user_password) VALUES(?,?,?)',
      [userData.userName,userData.userEmail,userData.userPassword]);

    if(result.affectedRows == 0)
    {
      throw new Error(messages.errorAddUser);
    }
    
    return result;
  }

  async updateUser(idUser,userData)
  {
    let result = await this.pool.query(
      'UPDATE user SET user_name = ?, user_email = ?, user_password = ? WHERE user_id = ?',
       [userData.userName,userData.userEmail,userData.userPassword,idUser]);
    
    if(result.affectedRows == 0)
    {
      throw new Error(messages.errorUpdateUser);
    }
    return result;
  }

  async deleteUser(idUser)
  {
    let result = await this.pool.query(
      'DELETE FROM user WHERE user_id = ?',
       [idUser]);

    if(result.affectedRows == 0)
    {
      throw new Error(messages.errorNoDeleteId);
    }
    return result;
  }
}

module.exports = new userDAO();